# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_OPTIONAL=1
GENTOO_DEPEND_ON_PERL="no"

inherit autotools perl-functions

MY_PV="$(ver_cut 1-2)"

DESCRIPTION="Library to support AppArmor userspace utilities"
HOMEPAGE="https://gitlab.com/apparmor/apparmor/wikis/home"
SRC_URI="https://gitlab.com/apparmor/apparmor/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENSE="GPL-2 LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~riscv ~x86"
IUSE="doc +perl static-libs"

RDEPEND="perl? ( dev-lang/perl:= )"
DEPEND="${RDEPEND}"
BDEPEND="
	dev-build/autoconf-archive
	sys-devel/bison
	sys-devel/flex
	doc? ( dev-lang/perl )
	perl? ( dev-lang/swig )
"
S=${WORKDIR}/apparmor-v${PV}-b4dfdf50f50ed1d64161424d036a2453645f0cfe/libraries/${PN}

# depends on the package already being installed
RESTRICT="test"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf \
		$(use_enable static-libs static) \
		$(use_with perl)
}

src_compile() {
	emake -C src
	emake -C include
	use doc && emake -C doc
	use perl && emake -C swig/perl
}

src_install() {
	emake DESTDIR="${D}" -C src install
	emake DESTDIR="${D}" -C include install
	use doc && emake DESTDIR="${D}" -C doc install

	if use perl ; then
		emake DESTDIR="${D}" -C swig/perl install
		perl_set_version
		insinto "${VENDOR_ARCH}"
		doins swig/perl/LibAppArmor.pm

		# bug 620886
		perl_delete_localpod
		perl_fix_packlist
	fi

	dodoc AUTHORS ChangeLog NEWS README

	find "${D}" -name '*.la' -delete || die
}
