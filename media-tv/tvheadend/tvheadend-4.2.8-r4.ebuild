# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit linux-info python-any-r1 systemd toolchain-funcs

DESCRIPTION="Tvheadend is a TV streaming server and digital video recorder"
HOMEPAGE="https://tvheadend.org/"
GIT_COMMIT="9ac61d7677feaf1078e2f3752cd8e580e2e61267"
SRC_URI="https://github.com/tvheadend/tvheadend/archive/${GIT_COMMIT}.tar.gz -> ${PN}-${GIT_COMMIT}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

IUSE="+capmt +constcw +cwc dbus debug dvben50221 +dvb +ffmpeg hdhomerun +imagecache +inotify iptv libressl satip systemd +timeshift tvhcsa uriparser xmltv zeroconf zlib"

BDEPEND="
	${PYTHON_DEPS}
	sys-devel/gettext
	virtual/pkgconfig
"

RDEPEND="
	acct-user/tvheadend
	virtual/libiconv
	media-libs/opus
	media-libs/x264
	media-libs/x265
	media-libs/libvpx
	dbus? ( sys-apps/dbus )
	tvhcsa? ( media-libs/libdvbcsa )
	dvben50221? ( media-tv/linuxtv-dvb-apps )
	ffmpeg? ( media-video/ffmpeg:0= )
	hdhomerun? ( media-libs/libhdhomerun )
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:= )
	uriparser? ( dev-libs/uriparser )
	zeroconf? ( net-dns/avahi )
	zlib? ( sys-libs/zlib )
"

DEPEND="
	${RDEPEND}
	dvb? ( sys-kernel/linux-headers )
"

RDEPEND+="
	dvb? ( media-tv/dtv-scan-tables )
	xmltv? ( media-tv/xmltv )
"

REQUIRED_USE="tvhcsa? ( || ( capmt constcw cwc dvben50221 ) )"

PATCHES=(
	"${FILESDIR}"/${PN}-4.0.9-use_system_queue.patch
	"${FILESDIR}"/${PN}-4.2.2-dtv_scan_tables.patch
	"${FILESDIR}"/${PN}-4.2.7-python3.patch
)

DOCS=( README.md )

S=${WORKDIR}/${PN}-${GIT_COMMIT}

pkg_setup() {
	python-any-r1_pkg_setup

	use inotify &&
		CONFIG_CHECK="~INOTIFY_USER" linux-info_pkg_setup
}

src_configure() {
	CC="$(tc-getCC)" \
	PKG_CONFIG="${CHOST}-pkg-config" \
	econf \
		--disable-bundle \
		--disable-ccache \
		--disable-dvbscan \
		--disable-ffmpeg_static \
		--disable-hdhomerun_static \
		--nowerror \
		$(use_enable capmt) \
		$(use_enable constcw) \
		$(use_enable cwc) \
		$(use_enable dbus dbus_1) \
		$(use_enable debug trace) \
		$(use_enable dvb linuxdvb) \
		$(use_enable tvhcsa) \
		$(use_enable dvben50221) \
		$(use_enable ffmpeg libav) \
		$(use_enable hdhomerun hdhomerun_client) \
		$(use_enable imagecache) \
		$(use_enable inotify) \
		$(use_enable iptv) \
		$(use_enable satip satip_server) \
		$(use_enable satip satip_client) \
		$(use_enable systemd libsystemd_daemon) \
		$(use_enable timeshift) \
		$(use_enable uriparser) \
		$(use_enable zeroconf avahi) \
		$(use_enable zlib)
}

src_compile() {
	emake CC="$(tc-getCC)"
}

src_install() {
	default

	newinitd "${FILESDIR}"/tvheadend.initd tvheadend
	newconfd "${FILESDIR}"/tvheadend.confd tvheadend

	use systemd &&
		systemd_dounit "${FILESDIR}"/tvheadend.service
}

pkg_postinst() {
	elog "The Tvheadend web interface can be reached at:"
	elog "http://localhost:9981/"
	elog
	elog "Make sure that you change the default username"
	elog "and password via the Configuration / Access control"
	elog "tab in the web interface."

	. "${EROOT}"/etc/conf.d/tvheadend &>/dev/null

	if [[ ${TVHEADEND_CONFIG} = ${EPREFIX}/etc/tvheadend ]]; then
		echo
		ewarn "The HOME directory for the tvheadend user has changed from"
		ewarn "${EPREFIX}/etc/tvheadend to ${EPREFIX}/var/lib/tvheadend. The daemon will continue"
		ewarn "to use the old location until you update TVHEADEND_CONFIG in"
		ewarn "${EPREFIX}/etc/conf.d/tvheadend. Please manually move your existing files"
		ewarn "before you do so."
	fi
}
