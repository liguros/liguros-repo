# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools cmake xdg-utils

DESCRIPTION="An old-school all-digital drum-kit sampler synthesizer with stereo fx"
HOMEPAGE="https://drumkv1.sourceforge.io/"
SRC_URI="https://github.com/rncbc/drumkv1/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"
RESTRICT="mirror"
LICENSE="GPL-2+"
SLOT="0"

IUSE="alsa debug lv2 osc standalone"
REQUIRED_USE="
	|| ( standalone lv2 )
	alsa? ( standalone )"

RDEPEND="
	dev-qt/qtbase:6
	dev-qt/qtsvg:6
	media-libs/libsndfile
	standalone? ( virtual/jack )
	alsa? ( media-libs/alsa-lib )
	lv2? ( media-libs/lv2 )
	osc? ( media-libs/liblo )
"
DEPEND="${RDEPEND}"

src_prepare() {
	cmake_src_prepare

	# Disable stripping
	echo "QMAKE_STRIP=" >> src/src_core.pri.in
	echo "QMAKE_STRIP=" >> src/src_jack.pri.in
	echo "QMAKE_STRIP=" >> src/src_ui.pri.in
	echo "QMAKE_STRIP=" >> src/src_lv2.pri.in

	default
}

src_configure() {
	local -a mycmakeargs=(
		-DCONFIG_DEBUG=$(usex debug yes no)
		-DCONFIG_JACK=$(usex standalone yes no)
		-DCONFIG_ALSA_MIDI=$(usex alsa yes no)
		-DCONFIG_LV2=$(usex lv2 yes no)
		-DCONFIG_LIBLO=$(usex osc yes no)
		-DCONFIG_QT6=yes
	)
	cmake_src_configure
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}
