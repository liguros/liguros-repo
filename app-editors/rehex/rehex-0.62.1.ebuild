# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

WX_GTK_VER="3.2-gtk3"
LUA_COMPAT=( lua5-{1,3,4} luajit )

inherit wxwidgets xdg lua-single

DESCRIPTION="Reverse Engineers' Hex Editor"
HOMEPAGE="https://github.com/solemnwarning/rehex"
SRC_URI="https://github.com/solemnwarning/rehex/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 ~arm64 x86"

RESTRICT="test"
LICENSE="GPL-2"
SLOT="0"
IUSE="doc"

RDEPEND="
	${LUA_DEPS}
	dev-libs/botan:2
	dev-libs/capstone
	dev-libs/jansson
	x11-libs/wxGTK:*[X]
"
BDEPEND="
	virtual/pkgconfig
	dev-lua/busted
	app-arch/zip
	doc? ( dev-perl/Template-Toolkit )
"
DEPEND="
	${RDEPEND}
	${BDEPEND}
"

src_configure() {
	export LUA_PKG=${ELUA}
	if use !doc ; then
		export BUILD_HELP=0
	fi
	setup-wxwidgets
}

src_install() {
	emake LUA_PKG=${ELUA} prefix="${D}"/usr install
}
