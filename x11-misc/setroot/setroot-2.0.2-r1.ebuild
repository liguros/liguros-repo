# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="simple X background setter inspired by imlibsetroot and feh"
HOMEPAGE="https://github.com/ttzhou/setroot"
SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="GPL-3"
SLOT="0"
IUSE="xinerama"

DEPEND="
	media-libs/imlib2
	x11-libs/libX11
	xinerama? ( x11-libs/libXinerama )
"
RDEPEND="${DEPEND}"
DOCS=( README.asciidoc )
PATCHES=(
	${FILESDIR}/imlib2-config.patch
)

src_compile() {
	emake xinerama=$(usex xinerama 1 0)
}

src_install() {
	einstalldocs
	emake DESTDIR="${D}" PREFIX="/usr" install
}
