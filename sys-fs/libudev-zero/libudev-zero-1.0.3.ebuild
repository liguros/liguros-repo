# Copyright 2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Daemonless replacement for libudev"
HOMEPAGE="https://github.com/illiliti/libudev-zero"
SRC_URI="https://github.com/illiliti/libudev-zero/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"

LICENSE="ISC"
SLOT="0"

RDEPEND="
	!sys-apps/systemd
	!sys-apps/systemd-utils[udev]
	!virtual/udev
	!sys-fs/eudev
"
DEPEND="sys-kernel/linux-headers"

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" LIBDIR="${EPREFIX}/usr/$(get_libdir)" install
}
