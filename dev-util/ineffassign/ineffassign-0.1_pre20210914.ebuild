# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
KEYWORDS="~amd64"

inherit go-module bash-completion-r1

DESCRIPTION="Detects ineffectual assignments in Go code."
HOMEPAGE="https://github.com/gordonklaus/ineffassign"
GIT_COMMIT="4cc7213b9bc8b868b2990c372f6fa057fa88b91c"
SRC_URI="
	https://github.com/gordonklaus/${PN}/archive/${GIT_COMMIT}.zip -> ${P}.zip
	https://gitlab.com/farout/liguros-distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="MIT License"
SLOT="0"
KEYWORDS="amd64 x86 arm"
S="${WORKDIR}/${PN}-${GIT_COMMIT}"

src_compile() {
	cd ${S}
	mkdir -pv bin || die
	go build -o "${S}/bin/${PN}" || die
}

src_install() {
	dobin bin/*
	dodoc LICENSE README.md
}
