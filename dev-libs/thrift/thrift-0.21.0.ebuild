# Copyright 2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

DESCRIPTION="C++ bindings for Apache Thrift"
HOMEPAGE="https://thrift.apache.org/lib/cpp.html"
SRC_URI="mirror://apache/thrift/${PV}/${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~loong ~ppc64 ~riscv ~s390 ~x86"
IUSE="libevent libressl lua +ssl test"

RESTRICT="!test? ( test )"

DEPEND="
	dev-libs/boost:=[nls(+)]
	!libressl? ( dev-libs/openssl:= )
	libressl? ( dev-libs/libressl:= )
	sys-libs/zlib:=
	libevent? ( dev-libs/libevent:= )
"
RDEPEND="${DEPEND}"
BDEPEND="
	app-alternatives/lex
	app-alternatives/yacc
"

REQUIRED_USE="
	test? ( ssl libevent )
"

PATCHES=(
	"${FILESDIR}/thrift-0.16.0-network-tests.patch"
	"${FILESDIR}/thrift-0.18.1-tests.patch"
)

src_prepare() {
	if use libressl; then
		eapply -p0 ${FILESDIR}/patch-lib_cpp_src_thrift_transport_TSSLSocket_cpp
	fi

	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_CPP=ON
		-DBUILD_C_GLIB=OFF
		-DBUILD_JAVA=OFF
		-DBUILD_JAVASCRIPT=OFF
		-DBUILD_NODEJS=OFF
		-DBUILD_PYTHON=OFF
		-DBUILD_TESTING=$(usex test 'ON' 'OFF')
		-DWITH_LIBEVENT=$(usex libevent 'ON' 'OFF')
		-DWITH_OPENSSL=$(usex ssl 'ON' 'OFF')
		-DWITH_ZLIB=ON
		-Wno-dev
	)
	cmake_src_configure
}

src_test() {
	MAKEOPTS="-j1" cmake_src_test
}
