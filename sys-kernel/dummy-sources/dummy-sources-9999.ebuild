# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="A dummy package for kernel sources."
HOMEPAGE="https://kernel.org"
SRC_URI=""

KEYWORDS="*"
LICENSE="GPL-2"

SLOT="0"

IUSE=""

PDEPEND="virtual/linux-sources"
