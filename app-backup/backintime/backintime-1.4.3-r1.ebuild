# Copyright 2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12,13} )
inherit python-single-r1 xdg

DESCRIPTION="Backup system inspired by TimeVault and FlyBack"
HOMEPAGE="https://backintime.readthedocs.io/en/latest/ https://github.com/bit-team/backintime/"

GIT_COMMIT="d33f35e10b4c3027b3b12881152c157eb1b2cc8c"
SRC_URI="https://github.com/bit-team/backintime/archive/${GIT_COMMIT}.tar.gz -> ${P}-${GIT_COMMIT}.tar.gz"
KEYWORDS="amd64 x86"

LICENSE="GPL-2"
SLOT="0"
IUSE="examples qt6 test"
RESTRICT="!test? ( test )"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="
	${PYTHON_DEPS}
	$(python_gen_cond_dep '
		dev-python/dbus-python[${PYTHON_USEDEP}]
		dev-python/keyring[${PYTHON_USEDEP}]
	')
"
RDEPEND="
	${DEPEND}
	virtual/openssh
	net-misc/rsync[xattr,acl]
	qt6? ( dev-python/PyQt6[gui,widgets] )
"
BDEPEND="
	sys-devel/gettext
	test? (
		$(python_gen_cond_dep '
			dev-python/pyfakefs[${PYTHON_USEDEP}]
		')
	)
"

PATCHES=( "${FILESDIR}/cleanup.patch" )

S=${WORKDIR}/${PN}-${GIT_COMMIT}

src_prepare() {
	default

	# Looks at host system too much, so too flaky
	rm common/test/test_tools.py || die
	# Fails with dbus/udev issue (likely sandbox)
	rm common/test/test_snapshots.py || die
}

src_configure() {
	pushd common > /dev/null || die
	# Not autotools
	./configure --python3 || die
	popd > /dev/null || die

	if use qt6 ; then
		pushd qt > /dev/null || die
		./configure --python3 || die
		popd > /dev/null || die
	fi
}

src_compile() {
	emake -C common

	if use qt6 ; then
		emake -C qt
	fi
}

src_test() {
	# pytest should work but it can't find the backintime binary, so
	# use the unittest-based runner instead.
	# https://github.com/bit-team/backintime/blob/dev/CONTRIBUTING.md#how-to-contribute-to-back-in-time
	emake -C common test-v
}

src_install() {
	emake -C common DESTDIR="${D}" install

	if use qt6 ; then
		emake -C qt DESTDIR="${D}" install
	fi

	einstalldocs

	if use examples ; then
		docinto examples
		dodoc common/{config-example-local,config-example-ssh}
	fi

	python_optimize "${D}"
}
