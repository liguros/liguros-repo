help([[
Set vim as your default editor.
]])

setenv("EDITOR", "/usr/bin/vi")
