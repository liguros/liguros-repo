# Copyright 2022-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake xdg

DESCRIPTION="Simple viewer for OpenEXR files"
HOMEPAGE="https://github.com/afichet/openexr-viewer"
SRC_URI="https://github.com/afichet/openexr-viewer/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-3"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~mips ppc ppc64 ~s390 sparc x86 ~amd64-linux ~x86-linux ~ppc-macos ~x86-macos"
IUSE=""

RDEPEND="
	dev-libs/imath
	media-libs/openexr
	sys-libs/zlib
"
DEPEND="
	${RDEPEND}
"

src_prepare() {
	# Removing faulty Version from desktop file
	sed -e "/Version/d" -i deploy/linux/openexr-viewer.desktop || die
	cmake_src_prepare
}

src_configure() {
	cmake_src_configure
}

src_install() {
	cmake_src_install
}


pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

