# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

CMAKE_MAKEFILE_GENERATOR=emake
inherit cmake

URI_DIR="Connector-C++"
DESCRIPTION="MySQL database connector for C++ (mimics JDBC 4.0 API)"
HOMEPAGE="https://dev.mysql.com/downloads/connector/cpp/"
SRC_URI="https://dev.mysql.com/get/Downloads/${URI_DIR}/${P}-src.tar.gz"
S="${WORKDIR}/${P}-src"

LICENSE="Artistic GPL-2"
SLOT="0"
KEYWORDS="amd64 arm ~arm64 -ppc ppc64 -sparc x86"
IUSE="+legacy libressl"

RDEPEND="
	!dev-db/mariadb
	app-arch/lz4:=
	app-arch/zstd:=
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.6.0:0= )
	sys-libs/zlib
	legacy? (
		dev-libs/boost:=
		virtual/mysql
	)
"
DEPEND="${RDEPEND}"

PATCHES=(
	"${FILESDIR}"/${PN}-8.0.27-mysqlclient_r.patch
	"${FILESDIR}"/${PN}-8.4.0-jdbc.patch
)

src_prepare() {
	cmake_src_prepare
	# ignores MAKEOPTS and runs recursive make -j$(nproc). Clobbers jobs badly
	# enough that your system immediately freezes.
	#
	# https://bugs.gentoo.org/921309
	# https://bugs.mysql.com/bug.php?id=115734
	sed -i 's/prc_cnt AND NOT/FALSE AND NOT/' cdk/cmake/dependency.cmake || die
}

src_configure() {
	local mycmakeargs=(
		-DBUNDLE_DEPENDENCIES=OFF
		# Cannot handle protobuf >23, bug #912797
		#-DWITH_PROTOBUF=system
		-DWITH_LZ4=system
		-DWITH_SSL=system
		-DWITH_ZLIB=system
		-DWITH_ZSTD=system
		-DWITH_JDBC=$(usex legacy)
	)

	if use legacy ; then
		mycmakeargs+=(
			-DMYSQLCLIENT_STATIC_BINDING=0
			-DMYSQLCLIENT_STATIC_LINKING=0
		)
	fi

	cmake_src_configure
}
