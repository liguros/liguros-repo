# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
POSTGRES_COMPAT=( 14 15 16 )

inherit postgres

DESCRIPTION="PostgreSQL Audit Extension"
HOMEPAGE="https://www.pgaudit.org/"
SRC_URI="https://github.com/pgaudit/pgaudit/archive/refs/tags/${PV}.tar.gz"

LICENSE="PostgreSQL"
SLOT="1.6"
KEYWORDS="~amd64"

DEPEND="dev-db/postgresql"
RDEPEND="${DEPEND}"

DOCS=( CHANGELOG.md README.md RELEASENOTES.md )

pkg_pretend() {
	postgres_check_slot
}

src_compile() {
	emake USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}

src_install() {
	emake DESTDIR="${D}" install USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}
