diff '--color=auto' -urN mysql.orig/vio/viosslfactories.cc mysql/vio/viosslfactories.cc
--- mysql.orig/vio/viosslfactories.cc	2025-01-25 22:49:16.337944272 +0100
+++ mysql/vio/viosslfactories.cc	2025-01-25 23:29:34.645095950 +0100
@@ -455,7 +455,7 @@
   }
 #endif /* HAVE_TLSv13 */
 
-#ifdef HAVE_TLSv13
+#if defined(HAVE_TLSv13) && !defined(LIBRESSL_VERSION_NUMBER)
   {
     /*
       Set suported signature algorithms for OpenSSL TLS v1.3
