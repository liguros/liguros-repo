# Copyright 2020-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12,13} )
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="Python tool for extracting information from PDF documents"
HOMEPAGE="https://pdfminersix.readthedocs.io/en/latest/"
SRC_URI="https://github.com/pdfminer/pdfminer.six/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc examples"

RDEPEND="
	>=dev-python/charset-normalizer-2.0.0[${PYTHON_USEDEP}]
	>=dev-python/cryptography-36.0.0[${PYTHON_USEDEP}]"

S=${WORKDIR}/${PN}.six-${PV}

distutils_enable_tests pytest

python_prepare_all() {
	sed -i -e "s:package.__version__:${PV}:" setup.py || die

	distutils-r1_python_prepare_all
}

python_compile_all() {
	use examples && emake -C samples all
}

python_install_all() {
	use doc && local HTML_DOCS=( docs/. )
	use examples && dodoc -r samples
	distutils-r1_python_install_all
}
