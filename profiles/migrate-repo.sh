#!/bin/bash

echo "Migrate from kits to single repo"

cd /var/git/

if [ -e /var/git/liguros-repo ]
then
    rm -rf /var/git/liguros-repo
fi

git clone https://gitlab.com/liguros/liguros-repo.git

cat <<EOF > /etc/portage/repos.conf/liguros.conf
[DEFAULT]
main-repo = liguros-repo

[liguros-repo]
location = /var/git/liguros-repo
auto-sync = yes
sync-type = git
sync-uri = https://gitlab.com/liguros/liguros-repo.git
priority = 1
EOF

sed -i 's/core-kit/liguros-repo/g' /etc/portage/make.profile/parent 
sed -i '/kits/d' /etc/portage/make.profile/parent

files=(/etc/portage/repos.conf/ego-*)
if [ -e "${files[0]}" ];
then
        rm /etc/portage/repos.conf/ego-*
fi

emerge -q1 =ego-3*
rm -rf /var/git/liguros-repo
ego sync

echo "You still need to change masters = core-kit to masters = liguros-repo in your local overlay/metadata/layout.conf"
