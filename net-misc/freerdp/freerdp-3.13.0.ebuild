# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake flag-o-matic

DESCRIPTION="Free implementation of the Remote Desktop Protocol"
HOMEPAGE="https://www.freerdp.com/"

MY_P=${P/_/-}
SRC_URI="https://pub.freerdp.com/releases/${MY_P}.tar.gz"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~ppc ~ppc64 ~x86"

LICENSE="Apache-2.0"
SLOT="3"
IUSE="aad alsa cpu_flags_arm_neon +client cups debug +ffmpeg +fuse gstreamer +icu jpeg kerberos libressl openh264 pulseaudio sdl server smartcard systemd test usb valgrind wayland X xinerama xv"
RESTRICT="!test? ( test )"
S="${WORKDIR}/${MY_P}"

BDEPEND="
	virtual/pkgconfig
	app-text/docbook-xsl-stylesheets
	dev-libs/libxslt
"
COMMON_DEPEND="
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )
	sys-libs/zlib:0
	aad? ( dev-libs/cJSON )
	alsa? ( media-libs/alsa-lib )
	cups? ( net-print/cups )
	usb? (
		virtual/libudev:0=
		sys-apps/util-linux:0=
		dev-libs/dbus-glib:0=
		virtual/libusb:1=
	)
	X? (
		x11-libs/libXcursor
		x11-libs/libXext
		x11-libs/libXi
		x11-libs/libXrender
		xinerama? ( x11-libs/libXinerama )
		xv? ( x11-libs/libXv )
	)
	ffmpeg? ( media-video/ffmpeg:0= )
	!ffmpeg? (
		x11-libs/cairo:0=
	)
	fuse? ( sys-fs/fuse:3 )
	gstreamer? (
		media-libs/gstreamer:1.0
		media-libs/gst-plugins-base:1.0
		X? ( x11-libs/libXrandr )
	)
	icu? ( dev-libs/icu:0= )
	jpeg? ( media-libs/libjpeg-turbo:0= )
	kerberos? ( virtual/krb5 )
	openh264? ( media-libs/openh264:0= )
	pulseaudio? ( media-libs/libpulse )
	sdl? (
		media-libs/libsdl2[haptic(+),joystick(+),sound(+),video(+)]
		media-libs/sdl2-ttf
	)
	server? (
		X? (
			x11-libs/libXcursor
			x11-libs/libXdamage
			x11-libs/libXext
			x11-libs/libXfixes
			x11-libs/libXrandr
			x11-libs/libXtst
			xinerama? ( x11-libs/libXinerama )
		)
	)
	smartcard? (
		dev-libs/pkcs11-helper
		sys-apps/pcsc-lite
	)
	systemd? ( sys-apps/systemd:0= )
	client? (
		wayland? (
			dev-libs/wayland
			x11-libs/libxkbcommon
		)
	)
	X? (
		x11-libs/libX11
		x11-libs/libxkbfile
	)
"
DEPEND="${COMMON_DEPEND}
	valgrind? ( dev-debug/valgrind )
"
RDEPEND="${COMMON_DEPEND}
	!net-misc/freerdp:0
	client? ( !net-misc/freerdp:2[client] )
	server? ( !net-misc/freerdp:2[server] )
	smartcard? ( app-crypt/p11-kit )
"

src_prepare() {
	if use libressl; then
		eapply -p1 $FILESDIR/freerdp-3.5.1.patch
		eapply -p1 $FILESDIR/smartcard_virtual_gids_c.patch
		eapply -p1 $FILESDIR/libfreerdp_crypto_tls_c.patch
	fi

	cmake_src_prepare
}

option() {
	usex "$1" ON OFF
}

option_client() {
	if use client; then
		option "$1"
	else
		echo OFF
	fi
}

src_configure() {
	# bug #881695
	filter-lto

	local mycmakeargs=(
		-Wno-dev

		# https://bugs.gentoo.org/927037
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=OFF

		-DBUILD_TESTING=$(option test)
		-DCHANNEL_URBDRC=$(option usb)
		-DWITH_AAD=$(option aad)
		-DWITH_ALSA=$(option alsa)
		-DWITH_CCACHE=OFF
		-DWITH_CLIENT=$(option client)
		-DWITH_CLIENT_SDL=$(option sdl)
		-DWITH_SAMPLE=OFF
		-DWITH_CUPS=$(option cups)
		-DWITH_DEBUG_ALL=$(option debug)
		-DWITH_MANPAGES=ON
		-DWITH_FFMPEG=$(option ffmpeg)
		-DWITH_FREERDP_DEPRECATED_COMMANDLINE=ON
		-DWITH_SWSCALE=$(option ffmpeg)
		-DWITH_CAIRO=$(option !ffmpeg)
		-DWITH_DSP_FFMPEG=$(option ffmpeg)
		-DWITH_FUSE=$(option fuse)
		-DWITH_GSTREAMER_1_0=$(option gstreamer)
		-DWITH_JPEG=$(option jpeg)
		-DWITH_KRB5=$(option kerberos)
		-DWITH_NEON=$(option cpu_flags_arm_neon)
		-DWITH_OPENH264=$(option openh264)
		-DWITH_OSS=OFF
		-DWITH_PCSC=$(option smartcard)
		-DWITH_PKCS11=$(option smartcard)
		-DWITH_PULSE=$(option pulseaudio)
		-DWITH_SERVER=$(option server)
		-DWITH_LIBSYSTEMD=$(option systemd)
		-DWITH_UNICODE_BUILTIN=$(option !icu)
		-DWITH_VALGRIND_MEMCHECK=$(option valgrind)
		-DWITH_X11=$(option X)
		-DWITH_XINERAMA=$(option xinerama)
		-DWITH_XV=$(option xv)
		-DWITH_WAYLAND=$(option_client wayland)
		-DWITH_WEBVIEW=OFF
		-DWITH_WINPR_TOOLS=$(option server)
	)
	cmake_src_configure
}

src_test() {
	local myctestargs=( -E TestBacktrace )
	has network-sandbox ${FEATURES} && myctestargs+=( -E TestConnect )
	cmake_src_test
}

src_install() {
	cmake_src_install
	mv "${ED}"/usr/share/man/man7/wlog{,3}.7 || die
}
