# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="Light Unix download accelerator"
HOMEPAGE="https://github.com/axel-download-accelerator/axel"
SRC_URI="https://github.com/axel-download-accelerator/axel/releases/download/v${PV}/${P}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~hppa ppc ppc64 sparc x86"
IUSE="debug wolfssl nls ssl"

CDEPEND="
	ssl? (
		!wolfssl? ( dev-libs/openssl:0= )
		wolfssl? ( dev-libs/wolfssl:0= )
	)
"
DEPEND="${CDEPEND}
	nls? ( sys-devel/gettext )"
RDEPEND="${CDEPEND}
	nls? ( virtual/libintl virtual/libiconv )"

DOCS=( doc/. )

src_configure() {
	local myeconfargs=(
		$(use_enable nls)
	)

	if use ssl; then
		if use wolfssl; then
			myeconfargs+=( --with-ssl=wolfssl )
		else
			myeconfargs+=( --with-ssl=openssl )
		fi
	fi

	econf "${myeconfargs[@]}"
}

pkg_postinst() {
	einfo 'To use axel with Portage, one can configure make.conf with:'
	einfo
	einfo 'FETCHCOMMAND="axel --timeout=30 --alternate --no-clobber --output=\"\${DISTDIR}/\${FILE}\" \"\${URI}\""'
	einfo 'RESUMECOMMAND="axel --timeout=30 --alternate --no-clobber --output=\"\${DISTDIR}/\${FILE}\" \"\${URI}\""'
}
