# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

LUA_COMPAT=( lua5-{1,3,4} luajit )

# do not add a ssl USE flag.  ssl is mandatory
SSL_DEPS_SKIP=1
inherit autotools flag-o-matic lua-single ssl-cert systemd toolchain-funcs

MY_P="${P/_/.}"
major_minor="$(ver_cut 1-2)"

if [[ ${PV} == *_rc* ]]; then
	rc_dir="rc/"
else
	rc_dir=""
fi

DESCRIPTION="An IMAP and POP3 server written with security primarily in mind"
HOMEPAGE="https://www.dovecot.org/"
SRC_URI="https://dovecot.org/releases/${major_minor}/${rc_dir}${MY_P}.tar.gz
	sieve? (
	https://pigeonhole.dovecot.org/releases/${major_minor}/${rc_dir}${PN}-pigeonhole-${PV}.tar.gz
	)
	managesieve? (
	https://pigeonhole.dovecot.org/releases/${major_minor}/${rc_dir}${PN}-pigeonhole-${PV}.tar.gz
	) "
S="${WORKDIR}/${MY_P}"
LICENSE="LGPL-2.1 MIT"
SLOT="0/${PV}"
KEYWORDS="~alpha ~amd64 arm ~arm64 ~hppa ~ia64 ~mips ppc ~ppc64 ~riscv ~s390 ~sparc ~x86"

IUSE_DOVECOT_AUTH="kerberos lua mysql pam postgres sqlite"
IUSE_DOVECOT_COMPRESS="lz4 zstd"
IUSE_DOVECOT_OTHER="argon2 caps doc libressl managesieve rpc selinux sieve solr static-libs stemmer suid systemd textcat unwind"

IUSE="${IUSE_DOVECOT_AUTH} ${IUSE_DOVECOT_COMPRESS} ${IUSE_DOVECOT_OTHER}"

REQUIRED_USE="lua? ( ${LUA_REQUIRED_USE} )"

DEPEND="
	app-arch/bzip2
	app-arch/xz-utils
	dev-libs/icu:=
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
	sys-libs/zlib:=
	virtual/libiconv
	argon2? ( dev-libs/libsodium:= )
	caps? ( sys-libs/libcap )
	kerberos? ( virtual/krb5 )
	net-nds/openldap:=
	lua? ( ${LUA_DEPS} )
	>=dev-cpp/clucene-2.3
	lz4? ( app-arch/lz4 )
	mysql? ( dev-db/mysql-connector-c:0= )
	pam? ( sys-libs/pam:= )
	postgres? ( dev-db/postgresql:* )
	rpc? ( net-libs/libtirpc:= net-libs/rpcsvc-proto )
	selinux? ( sec-policy/selinux-dovecot )
	solr? ( net-misc/curl dev-libs/expat )
	sqlite? ( dev-db/sqlite:* )
	stemmer? ( dev-libs/snowball-stemmer:= )
	suid? ( acct-group/mail )
	systemd? ( sys-apps/systemd:= )
	sys-apps/tcp-wrappers
	textcat? ( app-text/libexttextcat )
	unwind? ( sys-libs/libunwind:= )
	zstd? ( app-arch/zstd:= )
	virtual/libcrypt:=
	"

RDEPEND="
	${DEPEND}
	acct-group/dovecot
	acct-group/dovenull
	acct-user/dovecot
	acct-user/dovenull
	net-mail/mailbase[pam?]
	"

pkg_setup() {
	use lua && lua-single_pkg_setup
	if use managesieve && ! use sieve; then
		ewarn "managesieve USE flag selected but sieve USE flag unselected"
		ewarn "sieve USE flag will be turned on"
	fi
}

src_prepare() {
	default

	if use libressl; then
		eapply -p1 ${FILESDIR}/src_lib-ssl-iostream_iostream-openssl_c.patch
	fi

	# bug 657108, 782631
	#elibtoolize
	eautoreconf

	# Bug #727244
	append-cflags -fasynchronous-unwind-tables
}

src_configure() {
	local conf=""

	if use postgres || use mysql || use sqlite; then
		conf="${conf} --with-sql"
	fi

	# turn valgrind tests off. Bug #340791
	VALGRIND=no \
	LUAPC="${ELUA}" \
	systemdsystemunitdir="$(systemd_get_systemunitdir)" \
	econf \
		--with-rundir="${EPREFIX}/run/dovecot" \
		--with-statedir="${EPREFIX}/var/lib/dovecot" \
		--with-moduledir="${EPREFIX}/usr/$(get_libdir)/dovecot" \
		--disable-rpath \
		--with-bzlib \
		--without-libbsd \
		--with-icu \
		$( use_with argon2 sodium ) \
		$( use_with caps libcap ) \
		$( use_with kerberos gssapi ) \
		$( use_with lua ) \
		$( use_with lz4 ) \
		$( use_with mysql ) \
		$( use_with pam ) \
		$( use_with postgres pgsql ) \
		$( use_with sqlite ) \
		$( use_with solr ) \
		$( use_with stemmer ) \
		$( use_with systemd ) \
		$( use_with textcat ) \
		$( use_with unwind libunwind ) \
		$( use_with zstd ) \
		$( use_enable static-libs static ) \
		${conf}

	if use sieve || use managesieve; then
		# The sieve plugin needs this file to be build to determine the plugin
		# directory and the list of libraries to link to.
		emake dovecot-config
		cd "../dovecot-pigeonhole-${PV}" || die "cd failed"
		econf \
			$( use_enable static-libs static ) \
			--localstatedir="${EPREFIX}/var" \
			--enable-shared \
			--with-dovecot="${S}" \
			$( use_with managesieve )
	fi
}

src_compile() {
	default
	if use sieve || use managesieve; then
		cd "../dovecot-pigeonhole-${PV}" || die "cd failed"
		emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}"
	fi
}

src_test() {
	# bug #340791 and bug #807178
	local -x NOVALGRIND=true

	default
	if use sieve || use managesieve; then
		cd "../dovecot-pigeonhole-${PV}" || die "cd failed"
		default
	fi
}

src_install() {
	default

	if use suid; then
		einfo "Changing perms to allow deliver to be suided"
		fowners root:mail "/usr/libexec/dovecot/dovecot-lda"
		fperms 4750 "/usr/libexec/dovecot/dovecot-lda"
	fi

	newinitd "${FILESDIR}"/dovecot.init-r6 dovecot

	rm -rf "${ED}"/usr/share/doc/dovecot

	dodoc AUTHORS NEWS README.md TODO
	dodoc doc/*.{txt,cnf,xml,sh}
	doman doc/man/*.{1,7}

	local conf="${ED}/etc/dovecot/dovecot.conf"
	local confd="${ED}/etc/dovecot/conf.d"

	# Install LDAP configuration
	if use sieve || use managesieve; then
		cd "../dovecot-pigeonhole-${PV}" || die "cd failed"
		emake DESTDIR="${ED}" install
		rm -rf "${ED}"/usr/share/doc/dovecot
		docinto sieve/rfc
		dodoc doc/rfc/*.txt
		docinto sieve/devel
		dodoc doc/devel/DESIGN
		docinto plugins
		dodoc doc/plugins/*.txt
		docinto extensions
		dodoc doc/extensions/*.txt
		docinto locations
		dodoc doc/locations/*.txt
		doman doc/man/*.{1,7}
	fi

	use static-libs || find "${ED}"/usr/lib* -name '*.la' -delete
}

pkg_postinst() {
	# Let's not make a new certificate if we already have one
	if ! [[ -e "${ROOT}"/etc/ssl/dovecot/server.pem && \
		-e "${ROOT}"/etc/ssl/dovecot/server.key ]];	then
		einfo "Creating SSL	certificate"
		SSL_ORGANIZATION="${SSL_ORGANIZATION:-Dovecot IMAP Server}"
		install_cert /etc/ssl/dovecot/server
	fi
}
