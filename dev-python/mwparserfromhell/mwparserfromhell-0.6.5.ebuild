# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit distutils-r1

DESCRIPTION="Python MediaWiki parser"
HOMEPAGE="https://github.com/earwig/mwparserfromhell/"
SRC_URI="https://github.com/earwig/mwparserfromhell/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="dev-python/setuptools[$PYTHON_USEDEP]"
RDEPEND=""
