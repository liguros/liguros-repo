# Copyright 2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit distutils-r1

DESCRIPTION="A Django application to retrieve user's IP address"
HOMEPAGE="https://github.com/mher/flower"
SRC_URI="https://github.com/mher/flower/archive/refs/tags/v${PV}.tar.gz -> ${P}.gh.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-python/django-1.11[${PYTHON_USEDEP}]
	>=dev-python/celery-5.0.5
	>=dev-python/prometheus-client-0.8.0
	dev-python/humanize
	dev-python/pytz
	>=dev-python/tornado-5.0.0
	<dev-python/tornado-7.0.0
"

python_test() {
	"${EPYTHON}" tests/manage.py test -v2 myapp || die "Tests failed with ${EPYTHON}"
}
